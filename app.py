
import numpy as np
from flask import request as naminami
from flask import Flask, jsonify, render_template
import pickle

print(naminami.__module__)
app = Flask(__name__)

model = pickle.load(open('model/linear_model.pkl','rb'))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    input_data = [float(x) for x in naminami.form.values()]
    final_features = [np.array(input_data)]
    prediction = model.predict(final_features)

    output = round(prediction[0], 2)
    return render_template('index.html', result ='Salary predicted: ${}'.format(output))

@app.route('/api', methods=['POST'])
def api():
    data = naminami.get_json(force=True)
    prediction = model.predict([np.array(list(data.values()))])
    
    output = prediction[0]
    return jsonify(output)

if __name__ == '__main__':
    app.run()
    
